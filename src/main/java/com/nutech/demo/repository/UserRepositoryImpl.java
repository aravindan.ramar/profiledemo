package com.nutech.demo.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.UserBean;

/**
 * @author Aravindan
 *
 */
@Repository
public class UserRepositoryImpl implements UserRepository {
	@Autowired
	private SessionFactory sessionFactory;

	public void addUser(UserBean user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	public List<UserBean> getUser() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<UserBean> list = session.createCriteria(UserBean.class).list();
		return list;
	}

	public UserBean findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = (UserBean) session.get(UserBean.class, id);
		return user;
	}

	public UserBean findByUsername(String username) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from UserBean where username=:username");
		query.setParameter("username", username);
		return (UserBean) query.uniqueResult();
	}

	public UserBean updateUser(UserBean userBean) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = findByUsername(userBean.getUsername());
		user.setName(userBean.getName());
		user.setPassword(userBean.getPassword());
		user.setCountry(userBean.getCountry());
		session.update(user);
		return user;
	}

	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = findById(id);
		session.delete(user);
	}

	@Override
	public UserBean loginUser(LoginBean loginBean) {
		Session session = sessionFactory.getCurrentSession();
		Criteria loginCriteria = session.createCriteria(UserBean.class);
		Criterion usernameCriterion = Restrictions.eq("username", loginBean.getUsername());
		Criterion passwordCriterion = Restrictions.eq("password", loginBean.getPassword());
		loginCriteria.add(Restrictions.and(usernameCriterion, passwordCriterion));
		UserBean user = (UserBean) loginCriteria.uniqueResult();
		return user;
	}

}
