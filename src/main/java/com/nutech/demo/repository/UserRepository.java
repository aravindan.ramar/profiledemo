package com.nutech.demo.repository;

import java.util.List;

import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.UserBean;

/**
 * @author Aravindan
 *
 */
public interface UserRepository {
	public void addUser(UserBean user);
	public List<UserBean> getUser();
	public UserBean findById(int id);
	public UserBean findByUsername(String username);
	public UserBean loginUser(LoginBean loginBean);
	public UserBean updateUser(UserBean user);
	public void delete(int id);
}
