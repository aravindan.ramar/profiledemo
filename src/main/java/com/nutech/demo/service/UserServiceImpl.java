package com.nutech.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.repository.UserRepository;

/**
 * @author Aravindan
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userDao;


	public List<UserBean> getUser() {
		return userDao.getUser();
	}

	public UserBean findById(int id) {
		return userDao.findById(id);
	}
	
	public UserBean findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public void createUser(UserBean user) {
		userDao.addUser(user);
	}

	public void deleteUserById(int id) {
		userDao.delete(id);
	}
	
	@Override
	public UserBean updateUser(UserBean userBean) {
		return userDao.updateUser(userBean);
	}

	@Override
	public UserBean loginUser(LoginBean loginBean) {
		UserBean user = userDao.loginUser(loginBean);
		return user;
	}

}
