package com.nutech.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.service.UserService;


/**
 * @author Aravindan
 *
 */
@RestController
@RequestMapping(value={"/user"})
public class UserController {
	@Autowired
	UserService userService;

    /**
     * 
     * Description = It will fetch the user details based on id
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserBean> getUserById(@PathVariable("id") int id) {
        System.out.println("Fetching User with id " + id);
        UserBean user = userService.findById(id);
        if (user == null) {
            return new ResponseEntity<UserBean>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UserBean>(user, HttpStatus.OK);
    }
    
    
    
	 /**
	  *@desc This method is to create a new user
	 * @param user
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping(value="/create",headers="Accept=application/json")
	 public ResponseEntity<String> createUser(@RequestBody UserBean user, UriComponentsBuilder ucBuilder){
	     System.out.println("Creating User "+user.getName());
	     ResponseEntity<String> response = null;
	     UserBean userExist = userService.findByUsername(user.getUsername());
	     HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
	     if(userExist == null) {
	    	 userService.createUser(user);
	    	 response = new ResponseEntity<String>("Registered Successfully",headers, HttpStatus.CREATED);
	     }else {
	    	 response = new ResponseEntity<String>("Already Exists",headers, HttpStatus.CONFLICT);
	     }
	     
	     return response;
	 }
	 
	 /**
	  * @desc this method is used for login
	 * @param loginBean
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping(value="/login",headers="Accept=application/json")
	 public ResponseEntity<String> loginUser(@RequestBody LoginBean loginBean, UriComponentsBuilder ucBuilder){
	     System.out.println("Login User "+loginBean.getUsername());
	     UserBean userBean = userService.loginUser(loginBean);
	     String loginStatus = userBean != null ? "success" : "failure";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(userBean.getId()).toUri());
	     return new ResponseEntity<String>(loginStatus, headers, HttpStatus.OK);
	 }

	
	 /**
	  * @desc list of all user
	 * @return task
	 */
	@GetMapping(value="/get", headers="Accept=application/json")
	 public List<UserBean> getAllUser() {	 
	  List<UserBean> tasks=userService.getUser();
	  return tasks;
	
	 }

	
	/**
	 * @desc update user 
	 * @param currentUser
	 * @return
	 */
	@PutMapping(value="/update", headers="Accept=application/json")
	public ResponseEntity<String> updateUser(@RequestBody UserBean currentUser)
	{
		System.out.println("updating user"+currentUser.getId());
		UserBean user = userService.updateUser(currentUser);
		if (user==null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	/**
	 * @desc delete user based on ids
	 * @param id
	 * @return
	 */
	@DeleteMapping(value="/{id}", headers ="Accept=application/json")
	public ResponseEntity<UserBean> deleteUser(@PathVariable("id") int id){
		UserBean user = userService.findById(id);
		if (user == null) {
			return new ResponseEntity<UserBean>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUserById(id);
		return new ResponseEntity<UserBean>(HttpStatus.NO_CONTENT);
	}
}
